/*This program will communicate to an arduino */
#include <SoftwareSerial.h>

#define echoPin 7 // Echo Pin
#define trigPin 8 // Trigger Pin
#define LEDPin 13

int distanceLimit = 25;
long duration, distance;

void setup() {
 Serial.begin (9600);
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT);
 pinMode(LEDPin, OUTPUT);
 
}

void loop() {
/* The following trigPin/echoPin cycle is used to determine the
 distance of the nearest object by bouncing soundwaves off of it. */ 
 digitalWrite(trigPin, LOW); 
 delayMicroseconds(2); 

 digitalWrite(trigPin, HIGH);
 delayMicroseconds(10); 
 
 digitalWrite(trigPin, LOW);

 /*Read a pulse when the echoPin value is HIGH. It returns the
   length of the pulse in microseconds from when it started as
   HIGH and then changed to LOW.*/
 duration = pulseIn(echoPin, HIGH);
 
 /*Calculate the distance (in cm) based on the speed of sound.
   The duration is the amount of time in microseconds for the 
   pulse to go from HIGH to LOW. This is converted to centimeters.
   The speed of sound is 344 m/s which converts to .0344 cm/s,
   which means 29 microseconds per centimeter. The duration must
   be divided in half to calculate the distance one way. This would
   be duration/29/2 = duration/58 */
 distance = duration/58;

 /* Send the distance to the computer using Serial protocol */
 Serial.println(distance);

 /*If the distance from the sensor is less than 25 cm, turn
   on LED light, indicating it is too close, otherwise turn
   the LED light off if distance exceeds 25 cm*/
 if (distance <= distanceLimit){
    digitalWrite(LEDPin, HIGH);
 }
 else {
    digitalWrite(LEDPin, LOW);
 }
 
 
 //Delay 50ms before next reading.
 delay(50);
}

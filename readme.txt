Sampling Ultrasonic Data

Class: Embedded Systems Programming
Group 6
Individual Task for Milestone 2

April Hudspeth


About the Program
-----------------
This program will take in sampled data read from an ultrasonic sensor and
interperet the results. Based on the distance calculated from the sensor,
an LED light will turn on or off if it meets or exceeds the set limit.

Installation
------------
Arduino IDE: https://www.arduino.cc/en/Main/Software
FTDI Drivers: http://www.ftdichip.com/FTDrivers.htm


Hardware
--------
Arduino Pro Mini
FTDI 
Ultrasonic Sensor
Breadboard
Connecting cables
Mini USB cable
Computer with USB port

Setup and Running the Program
-----------------------------
1. Install software and drivers on computer
2. Hook Arduino Mini Pro on breadboard
3. Connect FTDI to Arduino Pro Mini (aligning up the pins correctly)
4. Connect Ultrasonic Sensor to Arduino Pro using connector cables
   Vcc  -> Vcc
   Trig -> Digital pin 8
   Echo -> Digital pin 7
   GND  -> GND
5. Plug mini USB into FTDI and the other end to computer's USB port
6. Load the program Distance_Measurement.ino source code included in this folder into a new Arduino sketch. 
7. Select the correct USB port via Tools -> Port
8. Upload the program to the Arduino.
9. View the data coming in by going to Tools -> Serial Monitor
10. Observe the LED pin 13 on Arduino Board as the ultrasonic sensor detects various distances
11. The LED light should turn ON for distances less than 25 centimeters and turn OFF for anything greater. 

